#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include "../inc/vptree.h"

#define N 10000
#define D 180
#define DEBUG 0
#define THRESHOLD 10000

void swap(double *a, double *b){
    double temp = *a;
    *a = *b;
    *b = temp;
}

int partition(double *A, int left, int right){

    double pivot = A[right];
    int pivotFinal = left;

    for (int x = left; x < right; x++){
        if (A[x] <= pivot){
            swap(&A[pivotFinal], &A[x]);
            pivotFinal++;
        }
    }

    swap(&A[pivotFinal], &A[right]);
    return pivotFinal;
}

double quickselect(double *A, int left, int right, int k){

    //p is position of pivot in the partitioned array
    int p = partition(A, left, right);

    if (p == k-1){
        return A[p];
    }
    //k less than pivot
    else if (p > k-1){
        return quickselect(A, left, p - 1, k);
    }
    //k greater than pivot
    else{
        return quickselect(A, p + 1, right, k);
    }
}

double findMedian(double array[], int n){
    if(n % 2 == 0){
        double temp1 = quickselect(array, 0, n-1, n/2);
        double temp2 = quickselect(array, 0, n-1, n/2 + 1);
        return (temp1 + temp2)/2;
    }
    return quickselect(array, 0, n-1, (n+1)/2);
}

void *calculateDistancesFromVP(vptree *T){
    double temp;
    if(T->n > THRESHOLD){
        cilk_for(int i=0; i<(T->n)-1; ++i){
            temp = 0;
            for(int j=0; j<(T->d); ++j){
                temp += pow((T->vp[j] - T->X[(T->d)*i + j]), 2);
            }
            T->distancesFromVP[i] = sqrt(temp);
        }
    }else{
        for(int i=0; i<(T->n)-1; ++i){
            temp = 0;
            for(int j=0; j<(T->d); ++j){
                temp += pow((T->vp[j] - T->X[(T->d)*i + j]), 2);
            }
            T->distancesFromVP[i] = sqrt(temp);
        }
    }


    return NULL;
}

int getIDX(vptree *T){
    return T->index;
};

double getMD(vptree * T){
    return T->medianDistance;
}

double *getVP(vptree * T){
    double *vp = (double *) malloc((T->d * sizeof(double)));
    int end = (T->n) * (T->d) - 1;
    for(int i=0; i<(T->d); ++i){
        vp[T->d - 1 -i] = T->X[end - i];
    }

    return vp;
}

void preorderPrint(vptree *T){
    if(T != NULL){
         printf("Vantage Point index: %d \n", getIDX(T));
         printf("Median distance from vantage point: %lf \n", T->medianDistance);
         printf("Inner size: %d, Outer size: %d\n", T->innerSize, T->outerSize);
         printf("Inner child: \n");
         preorderPrint(T->inner);
         printf("Outer child: \n");
         preorderPrint(T->outer);
    }else{
        printf("NULL Node");
    }
    printf("\n");
}

vptree *buildVPNode(double *X, int *indices, int n, int m){
    vptree *node = (vptree *) malloc(sizeof(vptree));
    if(n == 0){
        return (vptree *) NULL;
    }else if(n == 1){
        node->n = n;
        node->d = m;
        node->X = X;
        node->indices = indices;
        node->medianDistance = -1;
        node->vp = getVP(node);
        node->index = indices[0];
        node->innerSize = 0;
        node->outerSize = 0;
        node->inner = (vptree *) NULL;
        node->outer = (vptree *) NULL;
        return node;
    }else{
        node->n = n;
        node->d = m;
        node->X = X;
        node->indices = indices;
        node->vp = getVP(node);
        node->index = indices[n-1];

        node->distancesFromVP = (double *) malloc((n-1) * sizeof(double));
        if(!node->distancesFromVP) printf("Could not allocate memory");

        calculateDistancesFromVP(node);
        double tempDistancesFromVp[n-1];
        for(int i=0; i<n-1; ++i){
            tempDistancesFromVp[i] = node->distancesFromVP[i];
        }
        node->medianDistance = findMedian(node->distancesFromVP, (node->n)-1);
        node->distancesFromVP = tempDistancesFromVp;

        int innerSize = 0;
        for(int i=0; i<n-1; ++i){
            if(node->distancesFromVP[i] <= node->medianDistance){
                innerSize += 1;
            }
        }
        int outerSize = (n-1) - innerSize;

        int innerIndices[innerSize], outerIndices[outerSize];
        double *innerPoints = (double *) malloc(innerSize * m * sizeof(double));
        double *outerPoints = (double *) malloc(outerSize * m * sizeof(double));
        int nextFreeInner = 0, nextFreeOuter = 0;
        for(int i=0; i<n-1; ++i){
            if(node->distancesFromVP[i] <= node->medianDistance){
                innerIndices[nextFreeInner] = node->indices[i];
                for(int j=0; j<m; ++j){
                    innerPoints[m*nextFreeInner + j] = node->X[m*i + j];
                }
                nextFreeInner += 1;
            }else{
                outerIndices[nextFreeOuter] = node->indices[i];
                for(int j=0; j<m; ++j){
                    outerPoints[m*nextFreeOuter + j] = node->X[m*i + j];
                }
                nextFreeOuter += 1;
            }
        }


        node->inner = (vptree *)malloc(sizeof(vptree));
        node->outer = (vptree *)malloc(sizeof(vptree));
        if(!node->inner || !node->outer) exit(2);

        node->innerSize = innerSize;
        node->outerSize = outerSize;

        node->inner = cilk_spawn buildVPNode(innerPoints, innerIndices, innerSize, node->d);

        node->outer = buildVPNode(outerPoints, outerIndices, outerSize, node->d);

        cilk_sync;


        return node;
    }
};

vptree *buildvp(double *X, int n, int m){
    int indices[n];
    for(int i=0; i<n; ++i){
        indices[i] = i;
    }

    return buildVPNode(X, indices, n, m);
}

// Function getInner() does not write anything in memory
vptree *getInner(vptree *node){
    return node->inner;
}

// Function getOuter does not write anthing in memory
vptree *getOuter(vptree *node){
    return node->outer;
}

//   int main(){
//       double *X = (double*) malloc(N * D * sizeof(double));
//       if(!X){
//           if(DEBUG) printf("Error allocating memory'\n");
//           return 1;
//       }

//       for(int i=0; i<N; ++i){
//           for(int j=0; j<D; ++j){
//               X[D*i + j] =  rand()%100;
//           }
//       }

//       vptree *root = buildvp(X, N, D);

//       if(DEBUG) preorderPrint(root);

//       free(X);
//       free(root);
//       X = NULL;
//       root = NULL;

//       return 0;
//   };
